import 'package:flutter/material.dart';
import 'Screens/welcome_screen.dart';
import 'package:flutter/services.dart';
import 'constants.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: kPrimaryColor,
    systemNavigationBarColor: kPrimaryColor,
  ));
  runApp(ChatApp());
}

class ChatApp extends StatelessWidget {
  const ChatApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        colorScheme: ColorScheme.dark(),
        fontFamily: 'Poppins',
      ),
      home: WelcomeScreen(),
    );
  }
}
