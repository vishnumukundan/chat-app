import 'package:flutter/material.dart';
import 'constants.dart';

// text field

class TextFieldCustom extends StatelessWidget {
  TextFieldCustom({@required this.labelText, @required this.obsecured});

  final labelText;
  final obsecured;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: TextField(
        obscureText: obsecured,
        keyboardType: TextInputType.visiblePassword,
        cursorColor: kSecondaryColor,
        decoration: InputDecoration(
          labelText: labelText,
          labelStyle: TextStyle(decoration: TextDecoration.none),
          floatingLabelBehavior: FloatingLabelBehavior.never,
          contentPadding: EdgeInsets.all(20),
          filled: true,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}


// ---------------------------------------------------------------------