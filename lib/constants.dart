// color

import 'dart:ui';

import 'package:flutter/cupertino.dart';

const kPrimaryColor = Color(0xff080e2c);
const kSecondaryColor = Color(0xfff3fdfb);
const kSecondaryShadeColor = Color(0xffd8e8e4);
const kAccentColor = Color(0xff44eabb);

const kTextColor = Color(0xffffffff);
const kTextnewColor = Color(0xffffffff);

// const kLightColor = Color(0xffFEFFDE);

// Size
const double kDefaultTextSize = 16;
const double kBrandTextSize = 20;
const double kHeadingTextSize = 16;

const double kDefaultFontWeight = 600;
const double kHeadingFontWeight = 700;
const double kBrandFontWeight = 900;

// const kFontFamily =

// text style
const kBrandTextStyle = TextStyle(
  color: kSecondaryColor,
  fontSize: 25,
  fontFamily: 'Poppins',
  fontWeight: FontWeight.w800,
  letterSpacing: 1,
);

const kButtonTextStyle = TextStyle(
  color: kPrimaryColor,
  fontSize: 16,
  fontFamily: 'Poppins',
  fontWeight: FontWeight.w600,
);
